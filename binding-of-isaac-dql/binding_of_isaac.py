# ai
import gym
import numpy as np
from gym import spaces

# utils
import os
import time
import itertools as it
import pyautogui as pygui
from glob import glob
import cv2
import math
from PIL import Image
from mss.linux import MSS as mss #Switch based on operating system

# threading
from threading import Thread
from queue import Queue
import subprocess
from subprocess import Popen

def whichDigit(image):
    lowestMSE = np.amax(cv2.matchTemplate(image,digits[0], cv2.TM_SQDIFF))
    closestDigit = 0
    for d in range(len(digits)):
        thisMSE = np.amax(cv2.matchTemplate(image,digits[d], cv2.TM_SQDIFF))
        if lowestMSE > thisMSE:
            lowestMSE = thisMSE
            closestDigit = d
    return closestDigit

digits = [] #images of digits
for d in range(10):
    digits.append(cv2.imread('./digits/{}.jpg'.format(d)))
    print(whichDigit(digits[d]))

# sends keypress to game asynchronously 
def sendKey(windowid,k,duration=400,keyProcess=None):
    #if (keyProcess != None): #kill prev keypress command
        #...
        ##keyProcess.terminate()
    if (k!=None):
        #print(' '.join('xdotool key --delay {} --window {} {}'.format(duration,windowid,k).split(' ')))
        keyProcess = Popen('xdotool key --delay {} --window {} {}'.format(duration,windowid,k).split(' '))
    return keyProcess

def diff(a,b):
    return math.dist(a,b)


class IsaacGym(gym.Env):
    """Custom Environment that follows gym interface."""

    metadata = {"render.modes": ["human"]}
    moveKeys = ['w','s',None]
    move2Keys = ['a','d',None]
    shootKeys = ['Right','Left','Down','Up',None]
    itemKeys = ['q','e','space',None]

    moveKey = None
    move2Key = None
    itemKey = None
    shootKey = None
    scrt = mss()

    k1, k2,k3,k4 = None, None, None, None

    #combinationOf2Actions = it.combinations(actionKeys,2)
    #actionKeys = []
    #for combination in combinationOf2Actions:
        #actionKeys.append(' '.join(combination))


    gameRunning = False

    def __init__(self, channels, height, width, timeoutEpochs=90, windowName='Isaac', startupTime=10, gamePath=r'/home/cam/.local/share/Steam/steamapps/common/The Binding of Isaac Rebirth/run-x64.sh'):
        super().__init__()
        # Define action and observation space
        # They must be gym.spaces objects
        # Example when using discrete actions:
        self.action_space = spaces.MultiDiscrete((len(self.moveKeys),len(self.move2Keys),len(self.shootKeys),len(self.itemKeys)))
        # Example for using image as input (channel-first; channel-last also works):
        self.observation_space = spaces.Box(low=0, high=255,
                                            shape=(channels, height, width), dtype=np.uint8)
        
        ### startup code ###
        self.gamePath = gamePath
        self.startupTime = startupTime
        self.timeoutEpochs = timeoutEpochs
        self.windowName = windowName.lower()
        self.epochs = 0
        self.aiResolution = (width,height)

        ### Env Handling ###
        self.needToReset = True
        #self.model = None

        ### REWARDS
        self.coins = 0
        self.keys = 0
        self.bombs = 1
        self.hearts = 3
        self.mapCompletion = 20
        self.screen = None
        self.reward = -1

    def step(self, action):
        self.epochs += 1
        # wait for a keypress to be done
        if self.k1: self.k1.wait()
        elif self.k2: self.k2.wait()
        elif self.k3: self.k3.wait()
        elif self.k4: self.k4.wait()
        if not self.needToReset and self.epochs > 1:
            dur = 550
            self.k1 = sendKey(self.windowID,self.moveKeys[action[0]], duration=dur)
            self.k2 = sendKey(self.windowID,self.move2Keys[action[1]],duration=dur)
            self.k3 = sendKey(self.windowID,self.shootKeys[action[2]],duration=dur)
            self.k4 = sendKey(self.windowID,self.itemKeys[action[3]], duration=dur)
        self.getCoins()
        self.getBombs()
        self.getKeys()
        self.getHearts()
        self.getMapCompletion()

        if not self.gameRunning:
            self.reset()
            self.gameRunning=True


        # returns the change in rewards
        obs = self.observe(aiResolution=self.aiResolution)
        d = self.done()
        if (d):
            self.needToReset = True
        self.reward = self.calcReward()
        print('coins:',self.coins,'bombs:',self.bombs,'keys:',self.keys,'hearts:',self.hearts,'exploration:',self.mapCompletion,'reward:',self.reward)
        return obs, (self.reward if (not d) else 0), d, {}

    # returns screenshot of game # FIXME: if not display 0 this breaks
    def observe(self,screenResolutions=[(1920,1080),(1920,1080)],targetDisplay=0,aiResolution=(256,256)):
        region = (0,0,screenResolutions[targetDisplay][0],screenResolutions[targetDisplay][1])
        #img = pygui.screenshot('HumanObserve.jpg',region=region)
        #self.screen = pygui.screenshot(region=region)
        imgRaw = self.scrt.grab(self.scrt.monitors[2])
        self.screen = Image.frombytes("RGB", imgRaw.size, imgRaw.bgra, "raw", "BGRX")
        aiImg = self.screen.resize(aiResolution)
        #try:
            #for f in glob('.screenshot*.png'):
                #os.remove(f)
        #except:
            #pass
        return np.rollaxis(np.asarray(aiImg), 2, 0) #returns (channel, width, height) np array
    
    #def setModel(self, model):
        #self.model = model

    def done(self):
        done = pygui.locate(needleImage='death_indicator.jpg',haystackImage=self.screen,region=(1746,845,4,5),confidence=0.95) != None
        print("Done: ", done)
        return done
    
    def saveAIScreenShot(self):
        try:
            self.screen.resize(self.aiResolution).save('AIObserve.jpg')
        except:
            pass

    def reset(self):
        menuDelay = 1.05
        self.epochs = 0
        if not self.gameRunning:
            # try to kill existing game
            subprocess.run(['pkill', self.windowName])
            time.sleep(1)
            # record existing matching window ids before launching
            beforeLaunchPotentialWindows = os.popen('xdotool search --name "{}"'.format(self.windowName))
            time.sleep(1)
            startPath = os.getcwd()
            os.chdir(os.path.dirname(self.gamePath))
            #subprocess.run(('./'+os.path.basename(self.gamePath)+' >/dev/null 2>&1').split(' '),capture_output=True) #no output wanted
            os.system('./'+os.path.basename(self.gamePath)+' >/dev/null 2>&1')
            os.chdir(startPath)

            # Wait for game to boot
            time.sleep(self.startupTime)

            # Record existing matching window ids after launching
            afterLaunchPotentialWindows = os.popen('xdotool search --name "{}"'.format(self.windowName))

            self.windowID = "".join(y for x, y in it.zip_longest(beforeLaunchPotentialWindows, afterLaunchPotentialWindows) if x != y)
            self.windowID = self.windowID.replace('\n','')
            print('xdotool window id: ', self.windowID)

            #Thread(self.getCoins, args=())
            #self.getBombs()
            #self.getKeys()
            #self.getHearts()
            #self.getMapCompletion()

            # Go to main menu
            k = sendKey(self.windowID,'space')
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'space',keyProcess=k)
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'Down',keyProcess=k) #delete save
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'space',keyProcess=k)
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'Right',keyProcess=k)
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'Right',keyProcess=k)
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'space',keyProcess=k)
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'space',keyProcess=k)
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'space',keyProcess=k) #select save
            time.sleep(menuDelay)
            k= sendKey(self.windowID,'space',keyProcess=k) #select character
            time.sleep(menuDelay)
            sendKey(self.windowID,'space',keyProcess=k) #start game
            time.sleep(menuDelay)
        elif not self.done(): #not see game over screen
            k = sendKey(self.windowID,'space') #Close screen just in case
            time.sleep(menuDelay)
            k = sendKey(self.windowID,'Escape',keyProcess=k) #start game
            time.sleep(menuDelay)
            k = sendKey(self.windowID,'Down',keyProcess=k) #start game
            time.sleep(menuDelay)
            k = sendKey(self.windowID,'space',keyProcess=k) #start game
            time.sleep(menuDelay)
            k = sendKey(self.windowID,'Up',keyProcess=k) #start game
            time.sleep(menuDelay)
            k = sendKey(self.windowID,'space',keyProcess=k) #start game
            time.sleep(menuDelay)
            k = sendKey(self.windowID,'space',keyProcess=k) #start game
            time.sleep(menuDelay)
        else:
            sendKey(self.windowID,'space')
        time.sleep(3) #wait for cutscene
        self.needToReset = False
        return self.observe(aiResolution=self.aiResolution)  # reward, done, info can't be included

    def render(self, mode="human"): #todo: remove or properly implement
        self.saveAIScreenShot()
        pass

    # Calculate the score of the game state
    def calcReward(self):
        return (10*self.coins+100*self.bombs+30*self.keys-self.epochs+self.mapCompletion/10)*self.hearts

    # returns number of coins we think player has
    def getCoins(self):
        digit = []
        digit.append(str(whichDigit(np.asarray(self.screen.crop((142,194,142+24,194+32))))))
        offset = 0
        if (digit[0] == '1'): offset = -8
        digit.append(str(whichDigit(np.asarray(self.screen.crop((166+offset,194,166+offset+24,194+32))))))
        #print('coins: ',''.join(digit))
        self.coins = int(''.join(digit))

    # returns number of coins we think player has
    def getBombs(self):
        digit = []
        digit.append(str(whichDigit(np.asarray(self.screen.crop((142,242,142+24,242+32))))))
        offset = 0
        if (digit[0] == '1'): offset = -8
        digit.append(str(whichDigit(np.asarray(self.screen.crop((166+offset,242,166+offset+24,242+32))))))
        #print('bombs: ',''.join(digit))
        self.bombs = int(''.join(digit))

    # returns number of coins we think player has
    def getKeys(self):
        digit = []
        img = self.screen.crop((142,290,142+24,290+32))
        digit.append(str(whichDigit(np.asarray(img))))
        offset = 0
        if (digit[0] == '1'): offset = -8
        img = self.screen.crop((166+offset,290,166+offset+24,290+32))
        digit.append(str(whichDigit(np.asarray(img))))
        #print('keys: ',''.join(digit))
        self.keys = int(''.join(digit))
    
    def getHearts(self):
        red = (232,0,0)
        soul = (81,98,140) 
        dark = (60,60,60)
        angel = (255,255,255)
        numRed = 0
        numSoul = 0
        numDark = 0
        numAngel = 0
        for i in range(12):
            x,y = (258+i%12*20+(i%12//2)*8,78+i//12*40) #FIXME: guarantee second row works
            p = self.screen.getpixel((x,y))
            #screen.putpixel((x,y),(angel))
            if diff(p,red) < 20:
                numRed+=0.5
            elif diff(p,soul) < 25:
                numSoul+=0.5
            elif diff(p,dark) < 25:
                numDark+=0.5
            elif diff(p,angel) < 25:
                numAngel+=0.5
        print(numRed,numSoul,numDark,numAngel)
        ## DEBUG Heart Containers
        #os.remove('hearts.png')
        #self.screen.save('hearts.png')

        self.hearts = numRed*1 + numSoul * 2 + numDark * 2.4 + numAngel * 4
    
    def getMapCompletion(self):
        p1 = (1610,72)
        p2 = (1800,246)
        m = self.screen.crop((p1[0],p1[1],p2[0],p2[1]))
        m = np.asarray(m)
        p1, p2 = np.asarray(p1), np.asarray(p2)
        dialateAmount = np.ones((5,5),'uint8')
        roomDetect = cv2.inRange(m,(10,10,22),(255,255,255))
        roomDetect = cv2.dilate(roomDetect,dialateAmount,iterations=1)
        contours, hierarchy = cv2.findContours(roomDetect, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        map_center = np.asarray(p2-p1) / 2
        groups = []
        for c in contours:
            #center
            M = cv2.moments(c)
            center_X = int(M["m10"] / M["m00"])
            center_Y = int(M["m01"] / M["m00"])
            contour_center = (center_X, center_Y)
    
            distances_to_center = (math.dist(map_center, contour_center))
    
            groups.append({'contour': c, 'center': contour_center, 'distance_to_center': distances_to_center})
        if (len(groups) != 0):
            groups = sorted(groups, key=lambda i: i['distance_to_center'])
            center = groups[0]['contour']
            self.mapCompletion = max(cv2.contourArea(center),self.mapCompletion)

    def close(self):
        k=sendKey(self.windowID,'Escape') #menu
        time.sleep(0.25)
        k= sendKey(self.windowID,'Down',keyProcess=k)   #select exit game
        time.sleep(0.25)
        k= sendKey(self.windowID,'space',keyProcess=k)  #
        time.sleep(0.25)
        k= sendKey(self.windowID,'Escape',keyProcess=k) #escape the game
        time.sleep(0.25)
        k= sendKey(self.windowID,'Escape',keyProcess=k) #
        time.sleep(0.25)
        k= sendKey(self.windowID,'Escape',keyProcess=k) #
        time.sleep(0.25)
        k= sendKey(self.windowID,'Escape',keyProcess=k) #
        os.system('pkill ' + self.windowName)

if __name__ == '__main__':
    from stable_baselines3.common.env_checker import check_env
    # It will check your custom environment and output additional warnings if needed
    env = IsaacGym(3,150,150)
    check_env(env)
    env.close()