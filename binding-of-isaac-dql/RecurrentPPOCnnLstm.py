from binding_of_isaac import IsaacGym as igym
#from sb3_contrib import QRDQN
from sb3_contrib import RecurrentPPO
from typing import Callable
#from stable_baselines3 import DQN

progress_remaining = 1.0

def linear_schedule(initial_value: float,final_value) -> Callable[[float], float]:
    """
    Linear learning rate schedule.

    :param initial_value: Initial learning rate.
    :return: schedule that computes
      current learning rate depending on remaining progress
    """
    def func(_progress_remaining: float) -> float:
        """
        Progress will decrease from 1 (beginning) to 0.

        :param progress_remaining:
        :return: current learning rate
        """
        return progress_remaining * (initial_value-final_value) + final_value

    return func

env = igym(3,150,150)

##del model # remove to demonstrate saving and loading

model = None
episodeDur = 5000

# Load saved model

try:
    model = RecurrentPPO.load("isaac",env=env,device='auto')
    print("Loaded Saved Model")
except:
    print("Failed to load a saved model")

# Create new model
if not model:
    model = RecurrentPPO("CnnLstmPolicy", env, verbose=1,device='auto',learning_rate=linear_schedule(0.1,0.0000001),n_steps=episodeDur)
    print("Created New Model")


#env.setModel(model)

episodes = 0
trainTime = 100000000
while episodes < trainTime: 
    model.learn(total_timesteps=episodeDur,log_interval=4,progress_bar=True)
    model.save('isaac')
    episodes += episodeDur
    progress_remaining = min(1,max(0,float(float(episodes)/trainTime)))
    env.reset()

# Doesn't work with rppo
#obs = env.reset()
#while True:
    #action, _states = model.predict(obs)
    #obs, reward, done, info = env.step(action)
    #if done:
        #obs = env.reset()
        #break